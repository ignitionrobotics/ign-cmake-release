# ign-cmake-release

The ign-cmake-release repository has moved to: https://github.com/ignition-release/ign-cmake-release

Until May 31st 2020, the mercurial repository can be found at: https://bitbucket.org/osrf-migrated/ign-cmake-release
